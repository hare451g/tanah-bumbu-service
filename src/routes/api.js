const express = require('express');
const router = express.Router();
// middlewares
const loggerMiddleware = require('../middlewares/logger');

// controllers
const accounts = require('../services/accounts');
const articles = require('../services/articles');
const roles = require('../services/roles');

router.use(loggerMiddleware.timeLog)

router.route('/account')
  .get(accounts.get)
  .post(accounts.register)
  .patch(accounts.activate);

router.route('/account/find')
  .post(accounts.findByUsername);

router.route('/account/:id')
  .get(accounts.getById)
  .delete(accounts.deactivate);

router.route('/article')
  .post(articles.post)
  .get(articles.get);

router.route('/role')
  .get(roles.get)
  .post(roles.create);

router.route('/role/:id')
  .get(roles.getById)
  .put(roles.update);

module.exports = router;