// get dotenv
require('dotenv').config();

// initialize express js
const express = require('express');
const models = require('./models/index.js');
const app = express();

// initialize routes
const apiRoutes = require('./routes/api');

// initialize db connection
const dbConnection = models.initializeConnection();

// use body-parser json type
app.use(express.json());
app.use('/api', apiRoutes);

// initialize server
app.listen(process.env.SERVICE_PORT, () => { 
    console.log(
      `${process.env.SERVICE_NAME} listening on ${process.env.SERVICE_HOST}:${process.env.SERVICE_PORT}`
    )
  }
);