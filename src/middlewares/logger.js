const moment = require('moment');

module.exports = {
  timeLog: (req, res, next) => {
    console.log('Time:', moment().format('D/M/Y H:mm:s'));
    next();
  }
};