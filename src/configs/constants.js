module.exports = {
  RESULT_EMPTY_STRING: '',

  STATUS_ERROR_SERVER: 500,
  STATUS_ERROR: 400,
  STATUS_ERROR_MALFORMED: 422,
  STATUS_FORBIDDEN: 403,
  STATUS_NOT_FOUND: 404,
  STATUS_SUCCESS: 200,
  STATUS_SUCCESS_NO_CONTENT: 204,

  ROLE_ID_PLATH_DEVELOPER: 0,
  ROLE_ID_PLATH_SERVICE: 1,
  ROLE_ID_PLATH_ADMIN: 2,
  ROLE_ID_PLATH_MEMBER: 3,
  ROLE_ID_PLATH_GUEST: 4,
}