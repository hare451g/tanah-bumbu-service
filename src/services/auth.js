/*
  id - The unique identifier for the data
  data - The data sent by the user (for creating and updating)
  params (optional) - Additional parameters, for example the authenticated user or the query
*/
module.export = {
  // Create Token
  login: async(req, res) => {

  },

  // Refresh token
  refresh: async(req, res) => {

  },

  // Disable Token
  logout: async (req, res) => {

  },
}
