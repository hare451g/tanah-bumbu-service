/*
  id - The unique identifier for the data
  data - The data sent by the user (for creating and updating)
  params (optional) - Additional parameters, for example the authenticated user or the query
*/
const Models = require('../models');
const dbConnection = Models.initializeConnection();
const RoleModels = Models.initializeModel('roles', dbConnection);

const responseUtils = require('../utils/response');
const httpStatus = require('../utils/httpStatusCodes');

module.exports = {
  // get all roles
  get: async (req, res) => {
    RoleModels
    .findAll()
    .then(roles => {
      if (roles.length > 0) {
        res.status(httpStatus.OK).json(
          responseUtils.composeResponse({
            message: `found ${roles.length} roles`,
            data: roles,
            error: false,
          })
        );
      } else {
        res.status(httpStatus.NOT_FOUND).json(
          responseUtils.composeResponse({
            message: `not found`,
            data: {},
            error: true,
          })
        );
      }
    })
    .catch(error => {
      res.status(httpStatus.BAD_REQUEST).json(
        responseUtils.composeResponse({
          message: `Failed when getting roles`,
          data: error,
          error: true,
        })
      );
    });
  },
  // get one role by roleId
  getById: async (req, res) => {
    RoleModels
    .findOne({where: {id: req.params.id}})
    .then(role => { 
      if(role) {
        res.status(httpStatus.OK).json(role);
      } else {
        res.status(httpStatus.NOT_FOUND).json(
          responseUtils.composeResponse({
            message: `role ${req.params.id} is not exist !`,
            data: {},
            error: true,
          })
        );
      }
    })
    .catch(error => {
      res.status(httpStatus.BAD_REQUEST)
      .json(
        responseUtils.composeResponse({
          message: `Failed when getting roles by id ${req.params.id}`,
          data: error,
          error: true,
        })
      );
    });
  },
  // create new role
  create: async (req, res) => {
    RoleModels
    .create(req.body)
    .then(role => {
      res.status(httpStatus.OK)
      .json(
        responseUtils.composeResponse({
          message: 'Successfully created new Role',
          data: role,
          error: false,
        })
      )
    })
    .catch(error => { res.status(httpStatus.BAD_REQUEST)
      .json(
        responseUtils.composeResponse({
          message: 'Error when inserting new role',
          data: error,
          error: true,
        })
      )
    });
  },
  // create new role
  update: async (req, res) => {
    RoleModels.update(
      req.body, 
      { 
        where: { id: req.params.id },
      },
    )
    .then(updatedRole => {
      res.status(httpStatus.OK)
      .json(
        responseUtils.composeResponse({
          message: `updated role by id ${req.params.id}`,
          data: req.body,
          error: false,
        })
      )
    })
    .catch(error => { res.status(httpStatus.BAD_REQUEST)
      .json(
        responseUtils.composeResponse({
          message: 'Error when update existing role',
          data: error,
          error: true,
        })
      )
    });
  },
}
