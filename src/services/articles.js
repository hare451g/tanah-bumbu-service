const Models = require('../models');
const dbConnection = Models.initializeConnection();
const ArticleModel = Models.initializeModel('articles', dbConnection);

module.exports = {
  // find by keywords and filters
  find: async (req, res) => {
    
  },
  
  // get one articles from articleId
  get: async (req, res) => {

  },

  // create new article
  post: async (req, res) => {
    // get body
    const body = req.body;

    // Insert into database
    ArticleModel
      .create(body)
      .then(newArticle => res.json(newArticle))
      .catch(error => res.json(error));
  },

  // update existing articles, selected by id
  update: async(id, data, params) => {

  },

  // disable article by article id
  publish: async (id) => {

  },

  // disable article by article id
  disable: async (id) => {

  },
}
