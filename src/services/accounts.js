const Models = require('../models');

const Op = Models.Op;
const dbConnection = Models.initializeConnection();
const AccountModels = Models.initializeModel('users', dbConnection);

const responseUtils = require('../utils/response');
const httpStatus = require('../utils/httpStatusCodes');
const roles = require('../utils/roles');
const crypto = require('../utils/crypto');

module.exports = {
  // find by keywords and filters
  findByUsername: async (req, res) => {
    AccountModels
    .findAll({
      where: {
        username: { [Op.like]: `%${req.body.username}%` }
      }
    })
    .then(accounts => {
      res.status(httpStatus.OK)
      .json(responseUtils.composeResponse({
        error: false,
        data: accounts,
        message: `Found ${accounts.length} registered accounts`
      }));
    })
    .catch(error => {
      res.status(httpStatus.NOT_FOUND)
      .json(responseUtils.composeResponse({
        error: true,
        data: `${error}`,
        message: `Not found`
      }));
    })
  },

  getById: async (req, res) => {
    AccountModels.findOne({
      where: { id: req.params.id }
    })
    .then(account => {
      res.status(httpStatus.OK)
      .json(
        responseUtils.composeResponse({
          error: false,
          message: `Found user with id ${req.params.id}`,
          data: account,
        })
      )
    })
    .catch(error => {
      res.status(httpStatus.OK)
      .json(
        responseUtils.composeResponse({
          error: true,
          message: `User with id ${req.params.id} is not Exists`,
          data: account,
        })
      )
    });
  },
  
  // get one articles from articleId
  get: async (req, res) => {
    AccountModels
    .findAll({
      attributes: ['id', 'username', 'roleId']
    })
    .then(accounts => {
      if (accounts.length >= 1) {
        res.status(httpStatus.OK)
        .json(
          responseUtils.composeResponse({
            error: false,
            data: accounts,
            message: `Found ${accounts.length} accounts`,
          })
        );
      } else {
        res.status(httpStatus.NOT_FOUND)
        .json(
          responseUtils.composeResponse({
            error: true,
            data: {},
            message: 'Accounts is not exists',
          })
        );
      }
    }).catch(error => {
      res.status(httpStatus.BAD_REQUEST)
      .send(responseUtils.composeResponse({
        error: true,
        data: `${error}`,
        message: `Could not satisfy request`,
      }))
    });
  },

  // create new article
  register: async(req, res) => {
    const username = req.body.username;
    const password = await crypto.generateHash(req.body.password, crypto.saltRounds)
      .then(hash => hash)
      .catch(error => res.status(httpStatus.MALFORMED_REQUEST)
        .json(
          responseUtils.composeResponse({
            error: true,
            message: `Couldn't create hash`,
            data: `${error}`,
          })
        ) 
      );

    AccountModels.find({
      where: { username: req.body.username },
    }).then(accounts => {
      if (accounts) {
        res.status(httpStatus.FORBIDDEN)
        .json(
          responseUtils.composeResponse({
            error: true,
            message: `username ${username} is already exists !`,
            data: {accounts}
          })
        );
      } else {
        AccountModels.create({
          username,
          password,
          roleId: roles.ADMIN,
        })
        .then(account => {
          res.status(httpStatus.CREATED)
          .json(
            responseUtils.composeResponse({
              error:false,
              message: 'New user has been registered',
              data: account,
            })
          );
        })
        .catch(error => {
          res.status(httpStatus.BAD_REQUEST)
          .json(
            responseUtils.composeResponse({
              error:true,
              message: `user couldn't registered`,
              data: `${error}`,
            })
          );
        });
      }
    }).catch(error => {
      res.status(httpStatus.BAD_REQUEST)
      .json(
        responseUtils.composeResponse({
          error: true,
          data: `${error}`,
          message: 'Could not satisfy request',
        })
      );
    });
  },

  // update existing articles, selected by id
  update: async(id, data) => {

  },

  // disable article by article id
  activate: async (id) => {

  },

  // disable article by article id
  deactivate: async (req, res) => {
    AccountModels.destroy({
      where: { id: req.params.id },
    })
    .then(account => {
      if (account > 0) {
        res.status(httpStatus.OK)
        .json(
          responseUtils.composeResponse({
            error: false,
            message: `deleted 1 user with id ${req.params.id}`,
            data: account,
          })
        )
      } else {
        throw new Error(`couldn't delete user with id ${req.params.id}`);
      }
    })
    .catch(error => res.status(httpStatus.UNAUTHORIZED)
      .json(
        responseUtils.composeResponse({
          error: false,
          message: `couldn't delete user with id ${req.params.id}`,
          data: `${error}`,
        })
      )
    )
  },
}
