require('dotenv').config();

const Sequelize = require('sequelize');

module.exports = {
  initializeConnection: () => new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: process.env.DB_DIALECT,
      insecureAuth: true,
    }
  ),

  onCheckConnection: (dbConnection) => {
    dbConnection.authenticate()
    .then(() => true)
    .catch(error => error)
    .done();
  },

  Op: Sequelize.Op,

  initializeModel: (name, dbConnection) => require(`./${name}`)(dbConnection, Sequelize),
};
