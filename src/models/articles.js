/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('articles', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'users',
        key: 'id'
      },
      field: 'userId'
    },
    typeId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'types',
        key: 'id'
      },
      field: 'typeId'
    },
    descriptions: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'descriptions'
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'title'
    },
    contents: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'contents'
    },
    thumbnails: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'thumbnails'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'articles'
  });
};
