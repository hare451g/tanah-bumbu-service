/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sessions', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'users',
        key: 'id'
      },
      field: 'userId'
    },
    accessToken: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'accessToken'
    },
    refreshToken: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'refreshToken'
    },
    expiry: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'expiry'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'sessions'
  });
};
