module.exports = {
  // Compose Response
  composeResponse: ({ error, message, data}) => {
    if (error) {
      return {
        error,
        message,
        data
      };
    } else {
      return {
        success: true,
        message,
        data,
      }
    }
  }
}