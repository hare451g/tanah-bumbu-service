const bcrypt = require('bcrypt');

module.exports = {
  saltRounds: 5,
  generateHash: (plainText, saltRounds) => bcrypt.hash(plainText, saltRounds),
  compareHash: (plainText, hash) => bcrypt.compare(plainText, hash),
};
