'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('articles',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        userId: {
          type: Sequelize.INTEGER,
          references: { model: 'users', key: 'id' }
        },
        typeId: {
          type: Sequelize.INTEGER,
          references: { model: 'types', key: 'id' }
        },
        descriptions: Sequelize.STRING,
        title: Sequelize.STRING,
        contents: Sequelize.TEXT,
        thumbnails: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('articles');
  }
};
