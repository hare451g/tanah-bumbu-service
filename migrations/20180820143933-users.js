'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        roleId:{
          type: Sequelize.INTEGER,
          references: { model: 'roles', key: 'id' }
        },
        username: Sequelize.STRING,
        password: Sequelize.STRING,
        isActivated: Sequelize.BOOLEAN,
        createdAt: {
          type: Sequelize.DATE
        },
        updatedAt: {
          type: Sequelize.DATE
        }
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};
